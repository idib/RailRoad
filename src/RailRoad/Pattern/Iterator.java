package RailRoad.Pattern;

/**
 * Created by idib on 13.10.17.
 */
public interface Iterator<T> {
	boolean hasNext();

	boolean hasPrev();

	Iterator<T> next();

	Iterator<T> prev();

	T get();
}
