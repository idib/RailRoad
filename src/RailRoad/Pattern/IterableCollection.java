package RailRoad.Pattern;

import RailRoad.RoadObject.DinamicObject.Wagon.Wagon;

/**
 * Created by idib on 13.10.17.
 */
public interface IterableCollection<T> {
	Iterator<Wagon> getIterator();
}
