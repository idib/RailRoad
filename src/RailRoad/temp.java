package RailRoad;

import RailRoad.RoadObject.DinamicObject.Train;
import RailRoad.RoadObject.DinamicObject.Wagon.Carriage;
import RailRoad.RoadObject.DinamicObject.Wagon.Engine;
import RailRoad.RoadObject.DinamicObject.Wagon.Locomotive;
import RailRoad.RoadObject.Route;
import RailRoad.RoadObject.StaticObject.Arrow;
import RailRoad.RoadObject.StaticObject.Road;
import RailRoad.RoadObject.StaticObject.Station.Station;
import RailRoad.RoadObject.Tick;
import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idib on 16.10.17.
 */
public class temp {
	public static void main(String[] args) {
		Point2D ap = new Point2D(2, 4);
		Arrow a = new Arrow(ap);
		Station A = new Station(ap, "Станция 1", a);
		Point2D bp = new Point2D(51, 56);
		Arrow b = new Arrow(bp);
		Station B = new Station(bp, "Станция 2", b);
		Route route = new Route(A, B);
		List<Point2D> m = new ArrayList<>();
		m.add(ap);
		m.add(bp);

		Road road = new Road(m, a, b, 5);

		Train train = new Train("Скорый 1");
		train.setRoute(route);
		train.setRoad(road);
		Engine engine = new Engine(300, 2, 3);
		train.addLocomotive(new Locomotive("d", 5, 23, 2323, 5, new Carriage(), engine));

		route.addItem(road);
		route.setTrain(train);

		List<Tick> ticks = new ArrayList<>();
		ticks.add(A);
		ticks.add(B);
		ticks.add(train);

		A.t.add(train);

		long time = 0;

		while (true) {
			for (Tick tick : ticks) {
				tick.tick(time, 1);
			}
			time++;
		}

	}
}
