package RailRoad;

import RailRoad.CanvasObjects.ContextMenuExtended;
import RailRoad.Controllers.*;
import RailRoad.RoadObject.RailRoadModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class MainController {
	private RailRoadModel Model;
	private ActionController actionController;
	private ActionController defaultController;

	@FXML
	private Text statusBar;
	@FXML
	private Pane canvasPanel;
	@FXML
	private Pane actionBar;

	@FXML
	private Pane propertiesBar;

	@FXML
	public void initialize(){
		Model = new RailRoadModel();
		actionController = defaultController = new DefaultController(Model, statusBar, canvasPanel);
		Group p = new ContextMenuExtended(0, 0);
		canvasPanel.getChildren().add(p);
	}



	public void ClickOnBoard(MouseEvent mouseEvent) {
		if (mouseEvent.getButton() == MouseButton.PRIMARY)
			actionController.clickOnBoard(mouseEvent);
		if (actionController.checkExit() || actionController.checkNotReady())
			actionController = defaultController;
	}

	@FXML
	public void CreatePolyLine() {
//		if (actionController.getClass() != CreatePolyLineController.class) {
//			System.out.println("Create:PolyLine");
//			actionController.interrupt();
//			actionController = new CreatePolyLineController(Model, statusBar, canvasPanel, actionBar);
//		}
	}

	@FXML
	public void CreateStation() {
//		if (actionController.getClass() != CreateStationController.class) {
//			System.out.println("Create:Station");
//			actionController.interrupt();
//			actionController = new CreateStationController(Model, statusBar, canvasPanel, actionBar, propertiesBar);
//		}
	}

	@FXML
	public void CreateTrain(ActionEvent actionEvent) {

	}
}