//package RailRoad.Controllers;
//
//
//import RailRoad.CanvasObjects.Commands;
//import RailRoad.CanvasObjects.Edge;
//import RailRoad.CanvasObjects.ContextMenuExtended;
//import RailRoad.RailRoadModel;
//import javafx.scene.input.MouseEvent;
//import javafx.scene.layout.Pane;
//import javafx.scene.text.Text;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by idib on 30.01.17.
// */
//
//public class CreatePolyLineController extends ActionController implements Commands {
//
//	private List<ContextMenuExtended> pointList;
//
//	private List<Edge> edges;
//
//	public CreatePolyLineController(RailRoadModel model, Text statusBar, Pane board, Pane actionBar) {
//		super(model, statusBar, board, actionBar, "../view/actionBar/CreateLine.fxml"); //todo create file "CreatePolyLine"
//		pointList = new ArrayList<>();
//		edges = new ArrayList<>();
//	}
//
//	Status nextStatus() {
//		switch (status) {
//			case Start:
//				status = Status.EnterNextPoint;
//				break;
//			case EnterNextPoint:
//				break;
//			case Wait:
//				break;
//			default:
//				status = Status.NotReady;
//				break;
//		}
//		return status;
//	}
//
//	@Override
//	public void clickOnBoard(MouseEvent mouseEvent) {
//		boolean inits = false;
//		switch (status) {
//			case Start:
//			case EnterNextPoint:
//				inits = true;
//				break;
//			default:
//				break;
//		}
//		if (inits) {
//			pointList.add(new ContextMenuExtended(mouseEvent.getX(), mouseEvent.getY(), this));
//			board.getChildren().add(pointList.get(pointList.size() - 1));
//			if (pointList.size() > 1) {
//				edges.add(new Edge(pointList.get(pointList.size() - 2), pointList.get(pointList.size() - 1)));
//				board.getChildren().add(edges.get(edges.size() - 1));
//				edges.get(edges.size() - 1).refresh();
//			}
//			nextStatus();
//			printStatus();
//		}
//	}
//
//	@Override
//	void printStatus() {
//		if (status != Status.NotReady) {
//			String mess;
//			switch (status) {
//				case Start:
//					mess = "Start, Pleas Enter ContextMenuExtended One";
//					break;
//				case EnterNextPoint:
//					mess = "Enter Next ContextMenuExtended";
//					break;
//				case Exit:
//					mess = "Ready";
//					break;
//				default:
//					mess = "Incorrect com.idib.MMOH.Controllers.Status";
//					break;
//			}
//			StatusBar.setText(mess);
//		}
//	}
//
//	@Override
//	public void interrupt() {
//		board.getChildren().removeAll(edges);
//		board.getChildren().removeAll(pointList);
//	}
//
//	@Override
//	public void access() {
//		//todo check status
//
//		for (Edge edge : edges) {
//			edge.removeObservable();
//		}
//		board.getChildren().removeAll(pointList);
//		status = Status.Exit;
//		//todo enter data in com.idib.MMOH.RailRoadModel
//	}
//
//	@Override
//	public void delete(Object p) {
//		int curP = pointList.indexOf(p);
//		if (curP > 0 && curP < pointList.size()) {
//			if (curP != edges.size())
//				edges.get(curP).setFirst(pointList.get(curP - 1));
//			edges.get(curP - 1).removeObservable();
//			board.getChildren().remove(edges.get(curP - 1));
//			board.getChildren().remove(pointList.get(curP));
//			edges.remove(curP - 1);
//			pointList.remove(curP);
//		} else {
//			if (curP == 0) {
//				edges.get(0).removeObservable();
//				board.getChildren().remove(edges.get(0));
//				board.getChildren().remove(pointList.get(0));
//				edges.remove(0);
//				pointList.remove(0);
//			} else {
//				int last = pointList.size() - 1;
//				edges.get(last - 1).removeObservable();
//				board.getChildren().remove(edges.get(last - 1));
//				board.getChildren().remove(pointList.get(last));
//				edges.remove(last - 1);
//				pointList.remove(last);
//			}
//		}
//	}
//}