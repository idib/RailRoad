package RailRoad.Controllers;

import RailRoad.RoadObject.RailRoadModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

import java.io.IOException;

/**
 * Created by idib on 07.01.17.
 */
public abstract class ActionController {
	protected Status status;
	protected RailRoadModel model;
	protected Text StatusBar;
	protected Pane board;
	protected Pane actionBar;
	protected String pathToFXML;

	public ActionController(RailRoadModel model, Text statusBar, Pane board) {
		this.model = model;
		StatusBar = statusBar;
		this.board = board;
		status = Status.Start;
	}

	public ActionController(RailRoadModel model, Text statusBar, Pane board, Pane actionBar, String pathToFXML) {
		this.model = model;
		StatusBar = statusBar;
		this.board = board;
		status = Status.Start;
		this.actionBar = actionBar;
		loadPane(actionBar, pathToFXML);
	}


	protected void loadPane(Pane Bar, String pathToFXML) {
		Parent root = null;
		try {
			FXMLLoader loaded = new FXMLLoader(getClass().getResource(pathToFXML));
			loaded.setController(this);
			root = loaded.load();
			Bar.getChildren().clear();
			Bar.getChildren().add(root);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	abstract Status nextStatus();

	public abstract void clickOnBoard(MouseEvent mouseEvent);

	abstract void printStatus();

	public abstract void interrupt();

	public boolean checkExit() {
		return status == Status.Exit;
	}

	public boolean checkNotReady() {
		return status == Status.NotReady;
	}

	public abstract void access();
}
