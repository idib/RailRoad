//package RailRoad.Controllers;
//
//import RailRoad.CanvasObjects.Commands;
//import RailRoad.CanvasObjects.Observable;
//import RailRoad.RailRoadModel;
//import javafx.fxml.FXML;
//import javafx.geometry.Point2D;
//import javafx.scene.control.TextField;
//import javafx.scene.input.MouseEvent;
//import javafx.scene.layout.Pane;
//import javafx.scene.text.Text;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by idib on 04.10.17.
// */
//public class CreateStationController extends ActionController implements Commands {
//
//	private Pane propertiesBar;
//
//	@FXML
//	private TextField fxName;
//
//	@FXML
//	private TextField fxDescription;
//
//
//	public CreateStationController(RailRoadModel model, Text statusBar, Pane board, Pane actionBar, Pane Properties) {
//		super(model, statusBar, board, actionBar, "../view/actionBar/CreateLine.fxml");
//		propertiesBar = Properties;
//		loadPane(Properties, "../view/propertiesBar/PropertiesRoom.fxml");
//	}
//
//	@Override
//	Status nextStatus() {
//		switch (status) {
//			case Start:
//				status = Status.EnterNextPoint;
//				break;
//			case EnterNextPoint:
//				break;
//			case Wait:
//				break;
//			default:
//				status = Status.NotReady;
//				break;
//		}
//		return status;
//	}
//
//	@Override
//	public void clickOnBoard(MouseEvent mouseEvent) {
//
//	}
//
//	@Override
//	void printStatus() {
//		if (status != Status.NotReady) {
//			String mess;
//			switch (status) {
//				case Start:
//					mess = "Start, Pleas Enter ContextMenuExtended One";
//					break;
//				case EnterNextPoint:
//					mess = "Enter Next ContextMenuExtended";
//					break;
//				case Exit:
//					mess = "Ready";
//					break;
//				default:
//					mess = "Incorrect Status";
//					break;
//			}
//			StatusBar.setText(mess);
//		}
//	}
//
//	@Override
//	public void interrupt() {
//
//	}
//
//	@Override
//	public void access() {
//		status = Status.Exit;
//		List<Point2D> points = new ArrayList<>();
//		if (model.addStation(points, fxName.getText())) {
//			System.out.println("done: Room");
//		}
//	}
//
//	@Override
//	public void delete(Observable p) {
//
//	}
//}
