package RailRoad.Controllers;

/**
 * Created by idib on 07.01.17.
 */
public enum Status {
	Start, EnterPointOne, EnterPointTwo, EnterNextPoint, Exit, NotReady, Ready, Wait
}
