package RailRoad.Controllers;

import RailRoad.RoadObject.RailRoadModel;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

/**
 * Created by idib on 07.01.17.
 */
public class DefaultController extends ActionController {

	public DefaultController(RailRoadModel model, Text statusBar, Pane board) {
		super(model, statusBar, board);
		status = Status.Wait;
	}

	@Override
	Status nextStatus() {
		return status = Status.Wait;
	}

	@Override
	public void clickOnBoard(MouseEvent mouseEvent) {
	}

	@Override
	void printStatus() {
		if (status != Status.NotReady) {
			String mess;
			switch (status) {
				case Wait:
					mess = "Wait";
					break;
				default:
					mess = "Incorrect Status";
					break;
			}
			StatusBar.setText(mess);
		}
	}

	@Override
	public void interrupt() {

	}

	@Override
	public void access() {

	}
}
