package RailRoad;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("view/MainView.fxml"));
            AnchorPane r = new AnchorPane(root);
            AnchorPane.setBottomAnchor(root, 0.);
            AnchorPane.setLeftAnchor(root, 0.);
            AnchorPane.setRightAnchor(root, 0.);
            AnchorPane.setTopAnchor(root, 0.);
            primaryStage.setTitle("RAIL__ROAD");
            primaryStage.setScene(new Scene(r, 900, 600));
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}