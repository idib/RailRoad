package RailRoad.RoadObject;

import RailRoad.RoadObject.StaticObject.StaticObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by idib on 16.10.17.
 */
public class GlobalMap {
	private static GlobalMap gm;
	private Map<StaticObject, List<StaticObject>> g;


	public GlobalMap() {
		g = new HashMap<>();
	}

	public static GlobalMap getGlobalMap() {
		if (gm == null)
			gm = new GlobalMap();
		return gm;
	}

	public void addRoute(StaticObject a, StaticObject b) {
		if (!g.containsKey(a))
			g.put(a, new ArrayList<>());
		g.get(a).add(b);
	}

	public void removeRoute(StaticObject a, StaticObject b) {
		if (!g.containsKey(a))
			g.get(a).remove(b);
	}



}
