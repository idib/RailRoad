package RailRoad.RoadObject.DinamicObject;

import RailRoad.RoadObject.StaticObject.Arrow;
import RailRoad.RoadObject.StaticObject.Road;

import static java.lang.Math.min;

/**
 * Created by idib on 12.10.17.
 */
public class NormalSpeed implements StrategySpeed {
	static final long spareTime = 5;
	static final double percentOfMax = 0.8;

	@Override
	public double speed(Train train, long curTime) {
		Road road = train.getRoad();
		double maxSpeed = min(road.getMaxSpeed(), train.getMaxSpeed()) * percentOfMax;
		double endSpeed = Arrow.MaxSpeed;

		double timeStop = train.getTimeToSpeed(train.getStopForce(), endSpeed);
		double length = road.getLength();
		double dist = train.getDistanceOfRoad();
		double curSpeed = train.getCurSpeed();
		double lastTime = (length - dist) / curSpeed;

		if (lastTime > spareTime + timeStop) {
			if (train.getCurSpeed() < maxSpeed) {
				return train.getCurSpeed() + train.getDeltaIncreaseSpeed();
			}
		} else {
			return train.getCurSpeed() - train.getDeltaDecreaseSpeed();
		}

		return 0;
	}

	@Override
	public long getTime(Train train, Road road) {
		return 0;//todo
	}
}
