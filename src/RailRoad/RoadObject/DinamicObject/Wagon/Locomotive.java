package RailRoad.RoadObject.DinamicObject.Wagon;


/**
 * Created by idib on 13.10.17.
 */
public class Locomotive extends Wagon {
	Engine engine;

	public Locomotive(String name, double maxSpeed, double weight, double maxWeight, double length, Carriage carriage, Engine engine) {
		super(name, maxSpeed, weight, maxWeight, length, carriage);
		this.engine = engine;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public double getMaxPower() {
		return engine.getMaxPower();
	}

	public double getMaxIncresSpeed(){
		return engine.getIncresSpeed();
	}
}
