package RailRoad.RoadObject.DinamicObject.Wagon;

/**
 * Created by idib on 12.10.17.
 */
public class Carriage {
	static double g = 9.81;
	double radiusWheel = 0.3; // ?
	double coofForceResist = 0.51;  // defolt = 0.51  типо кооф силы трения качения
	double coofBrake = 0.3;  // defolt = 0.03 - 0.09
	int countWheels = 4;  // 4
	double trackGauge;  // ?



	//типо сила трения качения
	double getForceResist(double weight) {
		return coofForceResist / radiusWheel * g * weight * countWheels;
	}

	double getStopForce(double weight) {
		return coofBrake * g * weight * countWheels;
	}
}
