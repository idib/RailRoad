package RailRoad.RoadObject.DinamicObject.Wagon;

/**
 * Created by idib on 13.10.17.
 */
public class Engine {
	double power;
	double incSpeed;
	double decSpeed;

	public Engine(double power, double incSpeed, double decSpeed) {
		this.power = power;
		this.incSpeed = incSpeed;
		this.decSpeed = decSpeed;
	}

	public double getMaxPower() {
		return power;
	}

	public double getIncresSpeed() {
		return incSpeed;
	}

	public double getDecSpeed() {
		return decSpeed;
	}
}
