package RailRoad.RoadObject.DinamicObject.Wagon;

import RailRoad.RoadObject.DinamicObject.DynamicObject;
import RailRoad.Pattern.Iterator;


/**
 * Created by idib on 08.10.17.
 */
public class Wagon extends DynamicObject implements Iterator<Wagon> {
	Wagon prev;
	Wagon next;
	Carriage carriage;
	double weightCargo;
	int countCarrige = 2;

	public Wagon(String name, double maxSpeed, double weight, double maxWeight, double length, Carriage carriage) {
		super(name, maxSpeed, weight, maxWeight, length);
		this.carriage = carriage;
	}

	public Wagon getPrev() {
		return prev;
	}

	public void setPrev(Wagon prev) {
		this.prev = prev;
	}

	public Wagon getNext() {
		return next;
	}

	public void setNext(Wagon next) {
		this.next = next;
	}

	@Override
	public double getWeight() {
		return super.getWeight() + weightCargo;
	}

	public double getWeightCargo() {
		return weightCargo;
	}

	public void setWeightCargo(double weightCargo) {
		if (weightCargo <= getMaxWeight())
			this.weightCargo = weightCargo;
	}

	@Override
	public boolean hasNext() {
		return next != null;
	}

	@Override
	public boolean hasPrev() {
		return prev != null;
	}

	@Override
	public Iterator<Wagon> next() {
		return next;
	}

	@Override
	public Iterator<Wagon> prev() {
		return prev;
	}

	@Override
	public Wagon get() {
		return this;
	}

	public double getStopForce() {
		return 2 * carriage.getStopForce(getWeight() / 2);
	}

	public double getForceResist() {
		return 2 * carriage.getForceResist(getWeight() / 2);
	}
}
