package RailRoad.RoadObject.DinamicObject;

import RailRoad.RoadObject.StaticObject.Road;

/**
 * Created by idib on 12.10.17.
 */


// взависимости от обстоятельств возвращает скорость
public interface StrategySpeed {
	double speed(Train train, long curTime);

	long getTime(Train train, Road road);
}
