package RailRoad.RoadObject.DinamicObject;

import RailRoad.RoadObject.IdObject;
import RailRoad.RoadObject.Route;
import RailRoad.RoadObject.StaticObject.Road;

import static java.lang.Math.abs;

/**
 * Created by idib on 12.10.17.
 */
public abstract class DynamicObject extends IdObject {
	private String name;
	private Route route;
	private double curSpeed;
	private double MaxSpeed;
	private double deltaIncreaseSpeed;
	private double deltaDecreaseSpeed;
	private double weight;
	private double maxWeight;
	private Road road;   // todo перевезать на route
	private double distanceOfRoad; // todo перевезать на route
	private double length;

	public DynamicObject(String name, double maxSpeed, double weight, double maxWeight, double length) {
		this.name = name;
		MaxSpeed = maxSpeed;
		this.weight = weight;
		this.maxWeight = maxWeight;
		this.length = length;
	}

	public double getTimeToStop(double force) {
		return getTimeDeltaSpeed(force, getCurSpeed(), 0);
	}

	public double getTimeDeltaSpeed(double force, double maxSpeed, double speed) {
		return Math.ceil(getWeight() / 2 / force * abs(maxSpeed * maxSpeed - speed * speed));
	}

	public double getTimeToSpeed(double force, double speed) {
		return getTimeDeltaSpeed(force, getCurSpeed(), speed);
	}

	public double getTimefromMaxToSpeed(double force) {
		return getTimeDeltaSpeed(force, getMaxSpeed(), 0);
	}

	public Road getRoad() {
		return road;
	}

	public void setRoad(Road road) {
		this.road = road;
	}

	public double getDistanceOfRoad() {
		return distanceOfRoad;
	}

	private void setDistanceOfRoad(double distanceOfRoad) {
		this.distanceOfRoad = distanceOfRoad;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCurSpeed() {
		return curSpeed;
	}

	protected void setCurSpeed(double curSpeed) {
		this.curSpeed = curSpeed;
	}

	public double getMaxSpeed() {
		return MaxSpeed;
	}

	protected void setMaxSpeed(double maxSpeed) {
		MaxSpeed = maxSpeed;
	}

	public double getWeight() {
		return weight;
	}

	protected void setWeight(double weight) {
		this.weight = weight;
	}

	public double getMaxWeight() {
		return maxWeight;
	}

	protected void setMaxWeight(double maxWeight) {
		this.maxWeight = maxWeight;
	}

	public double getDeltaIncreaseSpeed() {
		return deltaIncreaseSpeed;
	}

	protected void setDeltaIncreaseSpeed(double deltaIncreaseSpeed) {
		this.deltaIncreaseSpeed = deltaIncreaseSpeed;
	}

	public double getDeltaDecreaseSpeed() {
		return deltaDecreaseSpeed;
	}

	protected void setDeltaDecreaseSpeed(double deltaDecreaseSpeed) {
		this.deltaDecreaseSpeed = deltaDecreaseSpeed;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public Double getLength() {
		return length;
	}

	protected void setLength(double length){
		this.length = length;
	}
}
