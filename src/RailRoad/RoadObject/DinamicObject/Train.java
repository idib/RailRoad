package RailRoad.RoadObject.DinamicObject;

import RailRoad.Pattern.IterableCollection;
import RailRoad.Pattern.Iterator;
import RailRoad.RoadObject.DinamicObject.Wagon.Locomotive;
import RailRoad.RoadObject.DinamicObject.Wagon.Wagon;
import RailRoad.RoadObject.Tick;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idib on 30.09.17.
 */
public class Train extends DynamicObject implements IterableCollection<Wagon>, Tick {
	private Wagon front, back;
	private List<Locomotive> locomotives;
	private StrategySpeed strategySpeed;
	private double stopForce;
	private double maxPower;
	private double forceResist;

	public Train(String name) {
		super(name, 0, 0, 0, 0);
		strategySpeed = new NormalSpeed();
		locomotives = new ArrayList<>();
	}

	private void calcWeight() {
		double curWeight = 0;
		Iterator<Wagon> cur = getIterator();
		while (cur != null) {
			curWeight += cur.get().getWeight();
			cur = cur.next();
		}
		setWeight(curWeight);
	}

	private void calcStopForce() {
		double curStopForce = 0;
		Iterator<Wagon> cur = getIterator();
		while (cur != null) {
			curStopForce += cur.get().getStopForce();
			cur = cur.next();
		}
		stopForce = curStopForce;
	}

	private void calcForceResist() {
		double curForceResist = 0;
		Iterator<Wagon> cur = getIterator();
		while (cur != null) {
			curForceResist += cur.get().getForceResist();
			cur = cur.next();
		}
		forceResist = curForceResist;
	}

	private void calcPower() {
		double power = 0;
		for (Locomotive locomotive : locomotives) {
			power += locomotive.getMaxPower();
		}
		maxPower = power;
	}

	private void calcDeltaSpeed() {
		setDeltaDecreaseSpeed(getMaxSpeed() / getTimefromMaxToSpeed(stopForce));
		setDeltaIncreaseSpeed(getMaxPower() / getForceResist());
	}

	private void calcMaxSpeed() {
		double maxSpeed = Double.MAX_VALUE;
		Iterator<Wagon> cur = getIterator();
		while (cur != null) {
			maxSpeed = Math.min(maxSpeed, cur.get().getMaxSpeed());
			cur = cur.next();
		}
		setMaxSpeed(maxSpeed);
	}

	private void calcLength() {
		double length = 0;
		Iterator<Wagon> cur = getIterator();
		while (cur != null) {
			length += cur.get().getLength();
			cur = cur.next();
		}
		setLength(length);
	}

	private void refresh() {
		calcWeight();
		calcStopForce();
		calcPower();
		calcForceResist();
		calcMaxSpeed();
		calcDeltaSpeed();
		calcLength();
	}

	public void addWagon(Wagon wagon) {
		if (front == null) {
			front = wagon;
		} else if (back == null) {
			back = wagon;
			front.setNext(back);
			back.setPrev(front);
		} else {
			back.setNext(wagon);
			wagon.setPrev(back);
			back = wagon;
		}
		refresh();
	}

	public void addLocomotive(Locomotive locomotive) {
		locomotives.add(locomotive);
		addWagon(locomotive);
	}

	public void tick(long time, long delta) {
		if (getRoute() != null) {
			double speed = strategySpeed.speed(this, time);
			//todo двигло
			double len = (speed + getCurSpeed()) / 2 * delta;
			setCurSpeed(speed);
			getRoute().progress(len);
			System.out.println(this);
		}
	}

	@Override
	public Iterator<Wagon> getIterator() {
		return front;
	}

	public double getStopForce() {
		return stopForce;
	}

	public double getForceResist() {
		return forceResist;
	}

	public double getMaxPower() {
		return maxPower;
	}

	@Override
	public String toString() {
		String str = "Поезд \"" + getName() + "\"";
		if (getRoute() != null)
			str += " Выполняет маршрут: " + getRoute().toString();
		return str;
	}
}
