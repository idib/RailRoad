package RailRoad.RoadObject;

import RailRoad.RoadObject.DinamicObject.DynamicObject;
import RailRoad.RoadObject.StaticObject.Arrow;
import RailRoad.RoadObject.StaticObject.Road;
import RailRoad.RoadObject.StaticObject.Station.Station;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by idib on 12.10.17.
 */
public class Route {
	private Station start;
	private Station finish;
	private double dist = 0;
	private double length = 0;
	private List<Road> transitional;
	private double curDist = 0;
	private int cur = 0;
	private DynamicObject train;
	private List<Arrow> blockeds;
	private List<Double> blockedsLen; // todo rename

	public Route(Station start, Station finish) {
		this.start = start;
		this.finish = finish;
		transitional = new ArrayList<>();
		blockeds = new ArrayList<>();
	}

	public void addItem(Road road) {
		transitional.add(road);
		length += road.getLength();
	}

	public void removeItem(Road road) {
		transitional.remove(road);
		length -= road.getLength();
	}

	public List<Road> getItems() {
		return transitional;
	}

	public void progress(double add) {
		boolean t = false; // todo как только поезд попал на станцию. последнии вогоны не должны сразу попасть на станцию
		if (curDist + add < getCur().getLength()) {
			curDist += add;
		} else {
			curDist = add - getCur().getLength() - curDist; // todo бага если будет короткий участок дороки то мы проскочим
		}
		dist += add;
		if (dist >= length) {
			finish.pushTrain(train);
			train.setRoute(null);
			t = true;
		}
		if (cur + 1 < transitional.size()) {
			Arrow arrow = getCommonArrow(transitional.get(cur), transitional.get(cur + 1));
			arrow.routing(train);
		}
		if (!blockeds.isEmpty()) {
			if (t) {
				for (Arrow blocked : blockeds) {
					blocked.unBlocked(train);
				}
			} else {
				for (int i = 0; i < blockeds.size(); i++) {
					if (dist >= train.getLength() + blockedsLen.get(i)) {
						blockeds.get(i).unBlocked(train);
						blockeds.remove(i);
						blockedsLen.remove(i);
						i--;
					}
				}
			}
		}
	}

	private Arrow getCommonArrow(Road a, Road b) {
		Set<Arrow> temp = new TreeSet<>();
		temp.add(a.getLeft());
		temp.add(a.getRight());
		if (temp.contains(b.getLeft()))
			return b.getLeft();
		if (temp.contains(b.getRight()))
			return b.getRight();
		return null;
	}

	@Override
	public String toString() {
		String str = "Маршрут " + start + "-" + finish;
		if (dist != 0 && length != 0)
			str += " выполнен на " + Math.min(Math.floor(dist / length * 100), 100) + "%";
		return str;
	}

	public Road getCur() {
		return (Road) transitional.get(cur);
	}

	public Road getNext() {
		if (transitional.size() > cur + 1)
			return (Road) transitional.get(cur + 1);
		return null;
	}

	public void addBlock(Arrow arrow) {
		blockeds.add(arrow);
		blockedsLen.add(dist);
	}

	public DynamicObject getTrain() {
		return train;
	}

	public void setTrain(DynamicObject train) {
		this.train = train;
	}
}
