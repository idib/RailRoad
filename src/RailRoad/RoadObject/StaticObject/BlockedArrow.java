package RailRoad.RoadObject.StaticObject;

import RailRoad.RoadObject.DinamicObject.DynamicObject;
import RailRoad.RoadObject.Route;
import RailRoad.RoadObject.StaticObject.Routing;
import javafx.util.Pair;

public class BlockedArrow implements Routing {
	private Arrow arrow;

	Pair<Road, Road> blockObject;
	Boolean blocked;


	public void routing(DynamicObject obj) {
		if (arrow.blocked){
			arrow.routing(obj);
		}
	}

	public boolean isBlocked(DynamicObject obj){
		return arrow.blocked;
	}
}