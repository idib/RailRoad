package RailRoad.RoadObject.StaticObject;

import RailRoad.RoadObject.Math.Func;
import javafx.geometry.Point2D;

import java.util.List;

/**
 * Created by idib on 30.09.17.
 */
public class Road extends CarcassObject {
	public static final double DistToArrow = 20;
	double trackGauge;
	double length;
	double maxSpeed;
	Arrow left, right;
	Func path;

	public Road(List<Point2D> circuit, Arrow left, Arrow right, double maxSpeed) {
		super(circuit);
		this.left = left;
		this.right = right;
		length = 0;
		this.maxSpeed = maxSpeed;
		Point2D last = null;
		for (Point2D point2D : circuit) {
			if (last != null)
				length += last.distance(point2D);
			last = point2D;
		}
		left.registeredRoad(this);
		right.registeredRoad(this);
	}

	public Arrow getLeft() {
		return left;
	}

	public void setLeft(Arrow left) {
		this.left.unregisteredRoad(this);
		this.left = left;
		left.registeredRoad(this);
	}

	public Arrow getRight() {
		return right;
	}

	public void setRight(Arrow right) {
		this.right.unregisteredRoad(this);
		this.right = right;
		right.registeredRoad(this);
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
}