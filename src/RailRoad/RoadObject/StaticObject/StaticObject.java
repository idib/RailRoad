package RailRoad.RoadObject.StaticObject;


import RailRoad.RoadObject.IdObject;
import javafx.geometry.Point2D;

/**
 * Created by idib on 09.09.16.
 */

abstract public class StaticObject extends IdObject {
	protected Point2D center;

	public StaticObject(Point2D center) {
		super();
		this.center = center;
	}

	public Point2D getCenter() {
		return center;
	}

	public double distToCenter(Point2D p) {
		return center.distance(p);
	}

	public double dist(Point2D p) {
		return distToCenter(p);
	}
}
