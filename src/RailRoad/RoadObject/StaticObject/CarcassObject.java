package RailRoad.RoadObject.StaticObject;

import javafx.geometry.Point2D;
import javafx.scene.shape.Line;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idib on 12.10.17.
 */
public abstract class CarcassObject extends StaticObject {
	protected List<Point2D> circuit;
	protected double minX;
	protected double minY;
	protected double maxX;
	protected double maxY;


	CarcassObject(List<Point2D> circuit) {
		super(circuit.get(0));
		this.circuit = circuit;


	}

	private Point2D calcCenter() {
		double sumX = 0;
		double sumY = 0;
		for (Point2D point : circuit) {
			double x = point.getX(), y = point.getY();
			sumX += x;
			sumY += y;
		}
		return center = new Point2D(sumX / (2 * circuit.size()), sumY / (2 * circuit.size()));
	}

	private void calcMinMax() {
		for (Point2D point : circuit) {
			double x = point.getX(), y = point.getY();
			minX = Math.min(x, minX);
			maxX = Math.max(x, maxX);
			minY = Math.min(y, minY);
			maxY = Math.max(y, maxY);
		}
	}

	public List<Point2D> getCircuit() {
		return circuit;
	}

	public void setCircuit(List<Point2D> circuit) {
		this.circuit = circuit;
		calcCenter();
		calcMinMax();
	}

	public double dist(Point2D p) {
		double min = p.distance(circuit.get(0));
		if (circuit.size() >= 2) {
			for (int i = 1; i < circuit.size(); i++) {
				min = Math.min(min, p.distance(circuit.get(i)));
				double pp2 = circuit.get(i).distance(p);
				double pp1 = circuit.get(i - 1).distance(p);
				double p1p2 = circuit.get(i - 1).distance(circuit.get(i));
				double x = -(pp2 * pp2 - pp1 * pp1) / p1p2;
				double y = Math.sqrt(pp1 * pp1 - x * x);
				min = Math.min(min, y);
			}
		}
		return min;
	}

	public List<Line> getAllLines() {
		ArrayList<Line> res = new ArrayList<>();
		for (int i = 1; i < circuit.size(); i++)
			res.add(new Line(
									circuit.get(i).getX(), circuit.get(i).getY(),
									circuit.get(i - 1).getX(), circuit.get(i - 1).getY()));
		return res;
	}
}
