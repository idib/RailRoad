package RailRoad.RoadObject.StaticObject.Station;

import RailRoad.RoadObject.DinamicObject.DynamicObject;
import RailRoad.RoadObject.DinamicObject.Train;
import RailRoad.RoadObject.StaticObject.Arrow;
import RailRoad.RoadObject.StaticObject.StaticObject;
import RailRoad.RoadObject.Tick;
import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idib on 05.10.17.
 */
public class Station extends StaticObject implements Tick {
	public static final double DistToRoad = 20;
	private String name;

	public Arrow getArrow() {
		return getArrow;
	}

	private Arrow getArrow;

	public List<Train> t = new ArrayList<>(); //todo delete

	public Station(Point2D center, String name, Arrow conntectToRoad) {
		super(center);
		this.name = name;
		getArrow = conntectToRoad;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	@Override
	public void tick(long time, long delta) {
		if (!t.isEmpty()) {
			Train train = t.get(0);
			train.setRoad(train.getRoute().getCur());
			t.remove(0);
			System.out.println("Отправлен поезд: " + train.toString());//todo delete

		}
	}


	public void pushTrain(DynamicObject train) {


		System.out.println("Прибыл поезд: " + train.toString());//todo delete
	}

	@Override
	public String toString() {
		return name;
	}
}
