package RailRoad.RoadObject.StaticObject;

import RailRoad.RoadObject.DinamicObject.DynamicObject;
import RailRoad.RoadObject.GlobalMap;
import RailRoad.RoadObject.Route;
import javafx.geometry.Point2D;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by idib on 10.10.17.
 */
public class Arrow extends StaticObject implements Routing {
	public static final double MaxSpeed = 5;
	private Set<Road> roads;
	private GlobalMap globalMap;

	public Pair<Road, Road> blockObject;
	public Boolean blocked;

	public Arrow(Point2D p) {
		super(p);
		roads = new HashSet<>();
		globalMap = GlobalMap.getGlobalMap();
	}

	public void registeredRoad(Road road) {
		roads.add(road);
	}

	public void unregisteredRoad(Road road) {
		roads.remove(road);
	}

	public void addRoute(Road a, Road b) {
		if (roads.contains(a) && roads.contains(b)) {
			globalMap.addRoute(a, b);
			globalMap.addRoute(b, a);
		}
	}

	public void removeRoute(Road a, Road b) {
		if (roads.contains(a) && roads.contains(b)) {
			globalMap.removeRoute(a, b);
			globalMap.removeRoute(b, a);
		}
	}

	public void routing(DynamicObject obj) {
		Route route = obj.getRoute();
		if (roads.contains(route.getCur()) && roads.contains(route.getNext())) {
			blocked = true;
			blockObject = new Pair<>(route.getCur(), route.getNext());
			route.addBlock(this);
		}
	}

	public void unBlocked(DynamicObject obj) {
		Route route = obj.getRoute();
		if (blockObject.equals(new Pair<>(route.getCur(), route.getNext()))) {
			blockObject = null;
			blocked = false;
		}
	}
}
