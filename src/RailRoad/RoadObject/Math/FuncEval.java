package RailRoad.RoadObject.Math;

import net.objecthunter.exp4j.Expression;

/**
 * Created by idib on 23.03.17.
 */
public class FuncEval extends Func {
    Expression e = null;

    public FuncEval(String name, Expression e) {
        super(name);
        this.e = e;
    }

    @Override
    public double calc(double x) {
        return e.setVariable("x", x).evaluate();
    }

}
