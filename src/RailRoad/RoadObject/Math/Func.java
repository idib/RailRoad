package RailRoad.RoadObject.Math;

import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idib on 16.04.17.
 */
public abstract class Func {
	public final double eps = 1e-5f;
	public String Name;
	public int countIter;
	public double step = 0.001;

	public Func(String name) {
		Name = name;
	}

	private boolean CheckNullable(double star, double end) {
		int n = (int) ((end - star) / step);
		for (int i = 1; i < n; i++) {
			double fn_ = calc(star + (step * (i - 1)));
			double fn = calc(star + (step * i));
			if (fn < 0 && fn_ < 0 || fn > 0 && fn_ > 0)
				return false;
		}
		return true;
	}

	private List<Double> derivative(List<Double> f) {
		int n = f.size() - 1;
		List<Double> res = new ArrayList<>();
		for (int i = 1; i < n; i++) {
			res.add((calc(f.get(i + 1)) - calc(f.get(i - 1))) / (2 * step));
		}
		return res;
	}

	private List<Double> getX(double star, double end) {
		int n = (int) (Math.abs(end - star) / step);
		List<Double> res = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			res.add(star + i * step);
		}
		return res;
	}

	public abstract double calc(double x);

	public Func derivative(double star, double end) {

		List<Double> x = getX(star, end);
		List<Double> y = derivative(x);
		x.remove(0);
		x.remove(x.size() - 1);
		return new FuncOnPoint(Name + '\'', x, y);
	}

	public double derivative(double x) {
		return (calc(x + eps) - calc(x - eps)) / (2 * eps);
	}

	public double findXSecant(double star, double end) {
		double next;
		countIter = 0;
		do {
			countIter++;
			next = star - (star - end) * calc(star) / (calc(star) - calc(end));
			end = star;
			star = next;
		} while (Math.abs(star - end) > eps);
		return star;
	}

	public double findXСhord(double star, double end, Func f, Func ffx) {
		double x = star, _x = end, last = end;
		double nextX, next_X;
		x = star;
		_x = end;


		countIter = 0;
		do {
			countIter++;
			if (f.calc(x) <= 0 && ffx.calc(x) <=  0)
				nextX = x - calc(x) / (calc(x) - calc(_x)) * (x - _x);
			else
				nextX = x - calc(x) / (f.calc(x));

			if (f.calc(_x) <= 0 && ffx.calc(_x) <= 0)
				next_X = _x - calc(_x) / (calc(_x) - calc(x)) * (_x - x);
			else
				next_X = _x - calc(_x) / (f.calc(_x));

			x = nextX;
			_x = next_X;

		} while (Math.abs(nextX - next_X) > eps);
		return nextX;
	}

//  todo
//    public XYSeries getPlot(double start, double finish) {
//        int n = 1000;
//        double step = Math.abs(finish - start) / n;
//        XYSeries series = new XYSeries(Name, false);
//        for (int i = 0; i < n; i++) {
//            double x = start + i * step;
//            series.add(x, calc(x));
//        }
//        return series;
//    }

	public List<Point2D> getPoints(double start, double finish) {
		int n = 1000;
		double step = Math.abs(finish - start) / n;
		List<Point2D> res = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			double x = start + i * step;
			res.add(new Point2D(x, calc(x)));
		}
		return res;
	}

	public double integrateRectangle(double star, double end) {
		double sum = Float.POSITIVE_INFINITY, last;
		double step = Math.abs(end - star);
		do {
			last = sum;
			step /= 2;
			sum = integrateRectangle(star, end, step);
		} while (Math.abs(last - sum) >= eps);
		this.step = step;
		return sum;
	}

	public double integrateRectangle(double star, double end, double step) {
		int n = (int) (Math.abs(end - star) / step);
		double sum = 0;
		countIter = 0;
		for (int i = 1; i <= n; i++) {
			sum += (calc(star + step * i));
			countIter++;
		}
		return sum * step;
	}

	public double integrateSimpson(double star, double end) {
		double sum = Float.POSITIVE_INFINITY, last;
		double step = Math.abs(end - star);
		do {
			last = sum;
			step /= 2;
			sum = integrateSimpson(star, end, step);
		} while (Math.abs(last - sum) >= eps);
		this.step = step;
		return sum;
	}

	public double integrateSimpson(double star, double end, double step) {
		int n = (int) (Math.abs(end - star) / step) - 1;
		double sum = 0;
		countIter = 0;
		for (int i = 1; i < n; i += 2) {
			countIter++;
			sum += (calc(star + step * (i - 1)) + 4 * calc(star + step * i) + calc(star + step * (i + 1)));
		}
		return sum * step / 3;
	}

	public double integrateTrapezoid(double star, double end) {
		double sum = Float.POSITIVE_INFINITY, last;
		double step = Math.abs(end - star);
		do {
			last = sum;
			step /= 2;
			sum = integrateTrapezoid(star, end, step);
		} while (Math.abs(last - sum) >= eps);
		this.step = step;
		return sum;
	}

	public double integrateTrapezoid(double star, double end, double step) {
		int n = (int) (Math.abs(end - star) / step);
		double sum = 0;
		countIter = 0;
		for (int i = 1; i < n; i++) {
			countIter++;
			sum += (calc(star + step * i) + calc(star + step * (i - 1))) / 2;
		}
		return sum * step;
	}
}
