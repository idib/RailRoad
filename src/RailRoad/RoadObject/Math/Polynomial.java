package RailRoad.RoadObject.Math;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by idib on 07.03.17.
 */
public class Polynomial extends Func {
    private List<Node> entry;

    public Polynomial(String name) {
        super(name);
    }

    public Polynomial(double a) {
        super("Polynomial");
        entry = new ArrayList<>();
        entry.add(new Node(a, 0));
    }

    public Polynomial(Node... a) {
        super("Polynomial");
        entry = new ArrayList<>();
        if (a.length == 0)
            entry.add(new Node(0, 0));
        for (Node node : a) {
            entry.add(node.clone());
        }
    }

    public Polynomial(String name, double a) {
        super(name);
        entry = new ArrayList<>();
        entry.add(new Node(a, 0));
    }

    public Polynomial(String name, Node... a) {
        super(name);
        entry = new ArrayList<>();
        if (a.length == 0)
            entry.add(new Node(0, 0));
        for (Node node : a) {
            entry.add(node.clone());
        }
    }

    public static Polynomial Lagrandg(List<Double> x, List<Double> y) {
        int n = x.size();
        Polynomial res = new Polynomial();
        for (int i = 0; i < n; i++) {
            double xk = x.get(i);
            Polynomial cur = new Polynomial(1);
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    double r = xk - x.get(j);
                    Node a = new Node(1 / r, 1);
                    Node b = new Node(x.get(j) / r, 0);
                    cur = mul(cur, sub(a, b));
                }
            }
            cur = mul(cur, y.get(i));
            res = add(res, cur);
        }
        res.Name = "Lagrandg";
        return res;
    }

    public static Polynomial Newton(List<Double> x, List<Double> y) {
        HashMap<Integer, HashMap<Integer, Double>> mem = new HashMap<>();
        int n = x.size();
        Polynomial res = new Polynomial(y.get(0));
        Polynomial cur = new Polynomial(1);
        for (int i = 1; i < n; i++) {
            cur = mul(cur, sub(new Node(1, 1), new Node(x.get(i - 1), 0)));
            res = add(res, mul(cur, Fij(0, i, x, y, mem)));
        }
        res.Name = "Newton";
        return res;
    }

    private static Polynomial add(Node a, Node b) {
        if (a.degress == b.degress) {
            a = a.clone();
            a.koo += b.koo;
            return new Polynomial(a);
        }
        return new Polynomial(a, b);
    }

    private static Polynomial sub(Node a, Node b) {
        if (a.degress == b.degress) {
            a = a.clone();
            a.koo -= b.koo;
            return new Polynomial(a);
        }
        b.koo *= -1;
        return new Polynomial(a, b);
    }

    private static Polynomial mul(Node a, Node b) {
        a = a.clone();
        a.koo *= b.koo;
        a.degress += b.degress;
        return new Polynomial(a);
    }

    public static Polynomial add(Polynomial a, Polynomial b) {
        a = a.clone();
        for (Node nb : b.entry) {
            a.entry.add(nb);
        }
        a.optimize();
        return a;
    }

    public static Polynomial mul(Polynomial a, Polynomial b) {
        Polynomial res = new Polynomial();
        for (Node na : a.entry) {
            for (Node nb : b.entry) {
                res = add(res, mul(na, nb));
            }
        }
        return res;
    }

    public static Polynomial mul(double b, Polynomial a) {
        return mul(a, b);
    }

    public static Polynomial mul(Polynomial a, double b) {
        a = a.clone();
        for (Node node : a.entry) {
            node.koo *= b;
        }
        return a;
    }

    private static double Fij(int i, int k, List<Double> x, List<Double> y, HashMap<Integer, HashMap<Integer, Double>> mem) {
        double res;
        if (mem.containsKey(i) && mem.get(i).containsKey(k))
            return mem.get(i).get(k);
        else if (k - i == 1) {
            res = (y.get(k) - y.get(i)) / (x.get(k) - x.get(i));
        } else {
            res = (Fij(i + 1, k, x, y, mem) - Fij(i, k - 1, x, y, mem)) / (x.get(k) - x.get(i));
        }
        if (!mem.containsKey(i))
            mem.put(i, new HashMap<>());
        mem.get(i).put(k, res);
        return res;
    }

    public void optimize() {
        entry.sort((o1, o2) -> Integer.compare(o2.degress, o1.degress));
        List<Node> res = new ArrayList<>();
        int n = entry.size();
        for (int i = 0; i < n; ) {
            int j = i + 1;
            while (j < n && entry.get(j).degress == entry.get(i).degress) {
                entry.get(i).koo += entry.get(j).koo;
                j++;
            }
            if (entry.get(i).koo != 0)
                res.add(entry.get(i));
            i = j;
        }
        entry = res;
    }

    public double calc(double x) {
        double sum = 0;
        for (Node node : entry) {
            sum += node.calc(x);
        }
        return sum;
    }

    @Override
    public Polynomial clone() {
        Polynomial a = new Polynomial();
        a.entry.clear();
        for (int i = 0; i < entry.size(); i++) {
            a.entry.add(entry.get(i).clone());
        }
        return a;
    }

    @Override
    public String toString() {
        String str = "";
        boolean fl = false;
        for (Node node : entry) {
            if (fl && node.koo > 0) {
                str += '+';
            }
            str += node.toString();
            fl = true;
        }
        return str;
    }

    static class Node {
        double koo;
        int degress;

        public Node(double koo, int degress) {
            this.koo = koo;
            this.degress = degress;
        }

        public boolean equals(Node obj) {
            return obj.koo == koo && obj.degress == degress ? true : false;
        }

        public Node clone() {
            return new Node(koo, degress);
        }

        public double calc(double x) {
            return (double) (koo * Math.pow(x, degress));
        }

        @Override
        public String toString() {
            String str = "";
            if (koo != 0) {
                str += koo;
                if (degress > 0)
                    str += "x";
                if (degress > 1)
                    str += "^" + degress;
            }
            return str;
        }
    }
}