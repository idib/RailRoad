package RailRoad.RoadObject.Math;

/**
 * Created by idib on 20.04.17.
 */
public class FuncCombine extends Func {
    Func f;

    public FuncCombine(Func f) {
        super(f.Name);
        this.f = f;
    }

    public FuncCombine(String name) {
        super(name);
    }

    public static FuncCombine add(Func a, Func b) {
        return new FuncCombine(new addFunc(a, b));
    }

    public static FuncCombine mul(Func a, Func b) {
        return new FuncCombine(new mulFunc(a, b));
    }

    public FuncCombine add(Func a) {
        return add(this, a);
    }

    public FuncCombine mul(Func a) {
        return mul(this, a);
    }

    public FuncCombine sub(Func a) {
        return add(this, a);
    }

    public FuncCombine dev(Func a) {
        return mul(this, a);
    }

    @Override
    public double calc(double x) {
        return f.calc(x);
    }

    private static class addFunc extends FuncCombine {
        Func f, s;

        public addFunc(Func a, Func b) {
            super(a.Name + '+' + b.Name);
            f = a;
            s = b;
        }

        @Override
        public double calc(double x) {
            return f.calc(x) + s.calc(x);
        }
    }

    private static class mulFunc extends FuncCombine {
        Func f, s;

        public mulFunc(Func a, Func b) {
            super('(' + a.Name + ")*(" + b.Name + ')');
            f = a;
            s = b;
        }

        @Override
        public double calc(double x) {
            return f.calc(x) * s.calc(x);
        }
    }

    private static class subFunc extends FuncCombine {
        Func f, s;

        public subFunc(Func a, Func b) {
            super(a.Name + '-' + b.Name);
            f = a;
            s = b;
        }

        @Override
        public double calc(double x) {
            return f.calc(x) - s.calc(x);
        }
    }

    private static class devFunc extends FuncCombine {
        Func f, s;

        public devFunc(Func a, Func b) {
            super('(' + a.Name + ")/(" + b.Name + ')');
            f = a;
            s = b;
        }

        @Override
        public double calc(double x) {
            return f.calc(x) / s.calc(x);
        }
    }
}
