package RailRoad.RoadObject.Math;

import javafx.geometry.Point2D;

import java.util.*;

/**
 * Created by idib on 16.04.17.
 */
public class FuncOnPoint extends Func implements List<Point2D> {
	List<Point2D> points = new ArrayList<>();

	public FuncOnPoint(String name) {
		super(name);
		points = new ArrayList<>();
	}

	public FuncOnPoint(String name, List<Double> x, List<Double> y) {
		super(name);
		for (int i = 0; i < x.size(); i++) {
			points.add(new Point2D(x.get(i), y.get(i)));
		}
	}

	public FuncOnPoint(String name, List<Point2D> points) {
		super(name);
		this.points = new ArrayList<>(points);
	}

	@Override
	public double calc(double x) {
		int n = points.size() - 1;
		for (int i = 0; i <= n; i++) {
			double x2 = points.get(i).getX();
			if (x == x2 || i == 0 && x < x2 || i == n && x > x2)
				return points.get(i).getY();
			if (i > 0 && x > points.get(i - 1).getX() && x < points.get(i).getX()) {
				double y2 = points.get(i).getY(), y1 = points.get(i - 1).getY();
				double x1 = points.get(i - 1).getX();
				double k = (y2 - y1) / (x2 - x1);
				return k * (x - x1) + y1;
			}
		}
		return Double.NaN;
	}

//  todo
//    @Override
//    public XYSeries getPlot(double start, double finish) {
//        XYSeries series = new XYSeries(Name, false);
//        for (int i = 0; i < points.size(); i++) {
//            series.add(points.get(i).getX(), points.get(i).getY());
//        }
//        return series;
//    }

	@Override
	public List<Point2D> getPoints(double start, double finish) {
		List<Point2D> res = new ArrayList<>();
		for (int i = 0; i < points.size(); i++) {
			if (points.get(i).getX() >= start && points.get(i).getX() <= finish)
				res.add(points.get(i));
		}
		return res;
	}

	public double maxX() {
		double max = Double.NEGATIVE_INFINITY;
		for (Point2D p : points) {
			max = Math.max(p.getX(), max);
		}
		return max;
	}

	public double maxY() {
		double max = Double.NEGATIVE_INFINITY;
		for (Point2D p : points) {
			max = Math.max(p.getY(), max);
		}
		return max;
	}

	public double minX() {
		double min = Double.POSITIVE_INFINITY;
		for (Point2D p : points) {
			min = Math.min(p.getX(), min);
		}
		return min;
	}

	public double minY() {
		double min = Double.POSITIVE_INFINITY;
		for (Point2D p : points) {
			min = Math.min(p.getY(), min);
		}
		return min;
	}

	public boolean pnpoly(Point2D point) {
		boolean res = false;
		int j = points.size() - 1;
		for (int i = 0; i < points.size(); i++) {
			if ((points.get(i).getY() < point.getY() && points.get(j).getY() >= point.getY() || points.get(j).getY() < point.getY() && points.get(i).getY() >= point.getY()) &&
					(points.get(i).getX() + (point.getY() - points.get(i).getY()) / (points.get(j).getY() - points.get(i).getY()) * (points.get(j).getX() - points.get(i).getX()) < point.getX()))
				res = !res;
			j = i;
		}
		return res;
	}

	@Override
	public int size() {
		return points.size();
	}

	@Override
	public boolean isEmpty() {
		return points.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return points.contains(o);
	}

	@Override
	public Iterator iterator() {
		return points.iterator();
	}

	@Override
	public Object[] toArray() {
		return points.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return points.toArray(a);
	}

	@Override
	public boolean add(Point2D point2D) {
		return points.add(point2D);
	}

	@Override
	public boolean remove(Object o) {
		return points.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return points.contains(c);
	}

	@Override
	public boolean addAll(Collection<? extends Point2D> c) {
		return points.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection<? extends Point2D> c) {
		return points.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return points.removeAll(c);
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		return points.retainAll(c);
	}

	@Override
	public void clear() {
		points.clear();
	}

	@Override
	public Point2D get(int index) {
		return points.get(index);
	}

	@Override
	public Point2D set(int index, Point2D element) {
		return points.set(index, element);
	}

	@Override
	public void add(int index, Point2D element) {
		points.add(index, element);
	}

	@Override
	public Point2D remove(int index) {
		return points.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		return points.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return points.lastIndexOf(o);
	}

	@Override
	public ListIterator listIterator() {
		return points.listIterator();
	}

	@Override
	public ListIterator listIterator(int index) {
		return points.listIterator(index);
	}

	@Override
	public List<Point2D> subList(int fromIndex, int toIndex) {
		return points.subList(fromIndex, toIndex);
	}
}
