package RailRoad.RoadObject;

/**
 * Created by idib on 12.10.17.
 */
public abstract class IdObject {
	private static boolean ready = false;
	private static long nextId;
	private final long id;

	public IdObject() {
		if (!ready)
			nextId = 0;
		id = nextId++;
	}

	public long getId() {
		return id;
	}
}
