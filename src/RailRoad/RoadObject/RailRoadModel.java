package RailRoad.RoadObject;

import RailRoad.RoadObject.StaticObject.Arrow;
import RailRoad.RoadObject.StaticObject.Road;
import RailRoad.RoadObject.StaticObject.Station.Station;
import javafx.geometry.Point2D;
import javafx.scene.shape.Line;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by idib on 24.10.16.
 */

public class RailRoadModel {
	private Set<Road> listRoad;
	private Set<Arrow> listArrows;
	private Set<Station> listStation;


	public RailRoadModel() {
		listArrows = new HashSet<>();
		listRoad = new HashSet<>();
		listStation = new HashSet<>();
	}

	private boolean detect() {
		Line2D t;

		return false;
	}

	private Arrow nearArrow(Point2D coord) {
		double min = Double.POSITIVE_INFINITY;
		Arrow res = null;
		for (Arrow road : listArrows) {
			double dist = road.dist(coord);
			if (min > dist) {
				min = dist;
				res = road;
			}
		}
		return res;
	}

	private Arrow nearOrNewArrow(Point2D coord, double maxDist) {
		Arrow near = nearArrow(coord);
		if (near == null || near.dist(coord) > maxDist) {
			near = new Arrow(coord);
		}
		return near;
	}

	public Road nearRoad(Point2D coord) {
		double min = Double.POSITIVE_INFINITY;
		Road res = null;
		for (Road road : listRoad) {
			double dist = road.dist(coord);
			if (min > dist) {
				min = dist;
				res = road;
			}
		}
		return res;
	}

	public boolean addStation(Station station) {
		Road road = nearRoad(station.getCenter());
		if (road.dist(station.getCenter()) < Station.DistToRoad) {
			listStation.add(station);
			station.getArrow().registeredRoad(road);
			addArrow(station.getArrow());
			return true;
		}
		return false;
	}

	public boolean addArrow(Arrow arrow) {
		if (listArrows.contains(arrow)) {
			listArrows.add(arrow);
			return true;
		}
		return false;
	}

	public boolean addRoad(Road road) {
		//// TODO: 16.10.17
//		for (Road r : listRoad) {
//			Point2D point = r.intersect(road);
//			if (point != null)
//				intersects.add(point);
//		}
		return false;
	}

	public Road createRoad(List<Point2D> circuit, double maxSpeed) {
		Point2D firstP = circuit.get(0);
		Point2D secondP = circuit.get(circuit.size() - 1);
		return new Road(circuit, nearOrNewArrow(firstP, Road.DistToArrow), nearOrNewArrow(secondP, Road.DistToArrow), maxSpeed);
	}

	public Arrow ctreateArrow(Point2D coord) {
		return new Arrow(coord);
	}

	public Station createStation(Point2D coord, String name) {
		Arrow near = nearOrNewArrow(coord, Station.DistToRoad);
		return new Station(coord, name, near);
	}
}