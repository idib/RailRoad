//package RailRoad.CanvasObjects;
//
//import javafx.scene.control.ContextMenu;
//import javafx.scene.control.MenuItem;
//import javafx.scene.shape.Ellipse;
//import javafx.scene.shape.Rectangle;
//
//import java.util.LinkedList;
//import java.util.List;
//
//public class StationShape extends Dragble<Rectangle>{
//	static double defaultRadius = 10;
//	private List<Observer> observerList;
//	private ContextMenu contextMenu;
//	private Commands command;
//
//	public StationShape(double centerX, double centerY, Commands _perent) {
//		super(new Rectangle(0, 0, defaultRadius, defaultRadius), centerX, centerY);
//		init(_perent);
//	}
//
//	public StationShape(double centerX, double centerY, double radius, Commands _perent) {
//		super(new Rectangle(0, 0, radius, radius), centerX, centerY);
//		init(_perent);
//	}
//
//	public void init(Commands _perent) {
//		command = _perent;
//		observerList = new LinkedList<>();
//		contextMenu = new ContextMenu();
//		MenuItem item1 = new MenuItem("delete");
//		item1.setOnAction(e ->
//				command.delete(this)
//		);
//
//		contextMenu.getItems().add(item1);
//	}
//
//	@Override
//	public void notifyObservers() {
//		for (Observer update : observerList) {
//			update.refresh();
//		}
//	}
//
//	@Override
//	public double getX() {
//		return node.getTranslateX();
//	}
//
//	@Override
//	public double getY() {
//		return node.getTranslateY();
//	}
//}