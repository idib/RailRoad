package RailRoad.CanvasObjects;

/**
 * Created by idib on 04.02.17.
 */
public interface Observer {
	public void refresh();
}
