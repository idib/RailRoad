package RailRoad.CanvasObjects;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.shape.Ellipse;

/**
 * Created by idib on 30.01.17.
 */
public class ContextMenuExtended extends Dragble<Ellipse> {
	static double defaultRadius = 10;
	private ContextMenu contextMenu;
	private Commands command;

	public ContextMenuExtended(double centerX, double centerY, Commands _perent) {
		super(new ObservableDragble(new Dragble(new Ellipse(0, 0, defaultRadius, defaultRadius), centerX, centerY)));

		// подумать

		command = _perent;
		contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("delete");
		item1.setOnAction(e ->
				command.delete(this)
		);

		contextMenu.getItems().add(item1);
		node.setOnContextMenuRequested(
				event -> contextMenu.show(node, event.getScreenX(), event.getScreenY()));
	}


	public ContextMenuExtended(double centerX, double centerY) {
		super((new Dragble(new Ellipse(0, 0, defaultRadius, defaultRadius), centerX, centerY)));

		// подумать

		contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("delete");

		contextMenu.getItems().add(item1);
		node.setOnContextMenuRequested(
				event -> contextMenu.show(node, event.getScreenX(), event.getScreenY()));
	}
}