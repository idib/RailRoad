package RailRoad.CanvasObjects;

import javafx.event.Event;
import javafx.scene.Group;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Shape;

/**
 * Created by idib on 05.10.17.
 */
public class Dragble<T extends Shape> extends Group {
    T node;
    private double mouseAnchorX;
    private double mouseAnchorY;
    private double initialTranslateX;
    private double initialTranslateY;

    public Dragble(T node, double centerX, double centerY) {
        super();
        this.node = node;
        getChildren().add(node);
        node.setTranslateX(centerX);
        node.setTranslateY(centerY);
        addEvents();
    }

    public Dragble(Dragble<T> dragble) {
        super();
        node = dragble.node;
        getChildren().add(node);
        node.setTranslateX(dragble.getX());
        node.setTranslateY(dragble.getY());
        addEvents();
    }

    public double getX() {
        return node.getTranslateX();
    }

    public double getY() {
        return node.getTranslateY();
    }

    private void addEvents() {
        node.addEventFilter(
                MouseEvent.ANY,
                Event::consume);

        node.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                mouseEvent -> {
                    mouseAnchorX = mouseEvent.getSceneX();
                    mouseAnchorY = mouseEvent.getSceneY();
                    initialTranslateX =
                            node.getTranslateX();
                    initialTranslateY =
                            node.getTranslateY();
                });
        node.addEventFilter(
                MouseEvent.MOUSE_DRAGGED,
                mouseEvent -> {
                    node.setTranslateX(
                            initialTranslateX
                                    + mouseEvent.getSceneX()
                                    - mouseAnchorX);
                    node.setTranslateY(
                            initialTranslateY
                                    + mouseEvent.getSceneY()
                                    - mouseAnchorY);
                    System.out.println("initialX " + initialTranslateX + " initialY" + initialTranslateY +
                                               "  mouseAnchorX " + mouseAnchorX + "  mouseAnchorY " + mouseAnchorY +
                                               "  mouseX " + mouseEvent.getSceneX() + "  mouseY " + mouseEvent.getSceneY());
                });
        node.removeEventFilter(MouseEvent.MOUSE_DRAGGED,
                mouseEvent -> {
                    node.setTranslateX(
                            initialTranslateX
                                    + mouseEvent.getSceneX()
                                    - mouseAnchorX);
                    node.setTranslateY(
                            initialTranslateY
                                    + mouseEvent.getSceneY()
                                    - mouseAnchorY);
                    System.out.println("initialX " + initialTranslateX + " initialY" + initialTranslateY +
                                               "  mouseAnchorX " + mouseAnchorX + "  mouseAnchorY " + mouseAnchorY +
                                               "  mouseX " + mouseEvent.getSceneX() + "  mouseY " + mouseEvent.getSceneY());
                });

        System.out.println(node.getEventDispatcher());
    }


}
