package RailRoad.CanvasObjects;

/**
 * Created by idib on 05.02.17.
 */
public interface Observable {
	public void registerObserver(Observer o);

	public void removeObserver(Observer o);

	public void notifyObservers();
}
