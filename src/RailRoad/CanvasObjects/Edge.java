//package RailRoad.CanvasObjects;
//
//import com.sun.istack.internal.NotNull;
//import javafx.scene.shape.Line;
//
///**
// * Created by idib on 30.01.17.
// */
//public class Edge extends Line implements Observer {
//
//	private ContextMenuExtended first;
//
//	private ContextMenuExtended second;
//
//	public Edge(ContextMenuExtended first, ContextMenuExtended second) {
//		super();
//		setFirst(first);
//		setSecond(second);
//	}
//
//	public Observable getFirst() {
//		return first;
//	}
//
//	public void setFirst(@NotNull ContextMenuExtended first) {
//		this.first.removeObserver(this);
//		this.first = first;
//		this.first.registerObserver(this);
//		refreshF();
//	}
//
//	public Observable getSecond() {
//		return second;
//	}
//
//	public void setSecond(@NotNull ContextMenuExtended second) {
//		this.second.removeObserver(this);
//		this.second = second;
//		this.second.registerObserver(this);
//		refreshS();
//	}
//
//	public void refresh() {
//		refreshF();
//		refreshS();
//	}
//
//	private void refreshF() {
//		setStartX(first.getX());
//		setStartY(first.getY());
//	}
//
//	private void refreshS() {
//		setEndX(second.getX());
//		setEndY(second.getY());
//	}
//
//	public void removeObservable() {
//		first.removeObserver(this);
//		second.removeObserver(this);
//	}
//}