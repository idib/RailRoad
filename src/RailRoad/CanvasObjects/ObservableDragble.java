package RailRoad.CanvasObjects;

import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idib on 09.10.17.
 */
public class ObservableDragble<T extends Shape> extends Dragble<T> implements Observable {
	Dragble<T> dragble;
	private List<Observer> observerList;

	public ObservableDragble(Dragble<T> dragble) {
		super(dragble);
		node.addEventFilter(MouseEvent.MOUSE_DRAGGED, event -> notifyObservers() );
		this.dragble = dragble;
		observerList = new ArrayList<>();
	}

	@Override
	public void registerObserver(Observer o) {
		if (!observerList.contains(o))
			observerList.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		if (observerList.contains(o))
			observerList.remove(o);
	}

	@Override
	public void notifyObservers() {
		for (Observer update : observerList)
			update.refresh();
		System.out.println("ObservableDragble");
	}


}
